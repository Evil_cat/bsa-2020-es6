import { controls } from '../../constants/controls';
import { CRTI_COOLDOWN } from '../../constants/fight';
// my previous try with a use of sleep function
// import sleep from '../helpers/sleep';

const {
  PlayerOneAttack,
  PlayerOneBlock,
  PlayerTwoAttack,
  PlayerTwoBlock,
  PlayerOneCriticalHitCombination,
  PlayerTwoCriticalHitCombination,
} = controls;

const playerOneButtons = {
  attack: PlayerOneAttack,
  block: PlayerOneBlock,
  crit: PlayerOneCriticalHitCombination,
};

const playerTwoButtons = {
  attack: PlayerTwoAttack,
  block: PlayerTwoBlock,
  crit: PlayerTwoCriticalHitCombination,
};

const fightStatus = {
  // object contains all pressed buttons
  pressedKeys: {},
};

// checks last crit time
const isCritOnCooldown = (lastCrit) => lastCrit && Date.now() - lastCrit < CRTI_COOLDOWN;

// returns winner if someones health below or equal zero, in all other cases returns 0
const getWinner = (firstFighter, secondFighter) => {
  if (firstFighter.currentHealth <= 0) {
    return secondFighter;
  } else if (secondFighter.currentHealth <= 0) {
    return firstFighter;
  } else {
    return null;
  }
}

// changes healthbar width
const updateHealthBar = (width, id) => {
  document.getElementById(id).style.width = width;
}

/**
 * Updates defender`s health if he is under attack
 * @param {Object} attacker
 * @param {Object} defender
 * @param {Object} attackerButtons
 * @param {Object} defenderButtons
 * @param {string} healthBarId
 */
const updateFighter = ({ 
  attacker,
  defender,
  attackerButtons,
  defenderButtons,
  healthBarId
}) => {
  if (!isCritOnCooldown(attacker.lastCrit) && attackerButtons.crit.every((val) => fightStatus.pressedKeys[val])) {
    defender.currentHealth -= getCritPower(attacker);
    attacker.lastCrit = Date.now();
  } else if (fightStatus.pressedKeys[attackerButtons.attack]) {
    const defencePower = getBlockPower(defender);
    const hitPower = getHitPower(attacker);

    if (fightStatus.pressedKeys[defenderButtons.block]) {
      defender.currentHealth -= getDamage(hitPower, defencePower);
    } else {
      defender.currentHealth -= hitPower;
    }
  }

  const { currentHealth, health } = defender;
  const playerOneHealthToShow = currentHealth > 0 ? currentHealth : 0;
  updateHealthBar(`${Math.ceil((playerOneHealthToShow * 100) / health)}%`, healthBarId);
}

const updateFight = (firstFighter, secondFighter) => {
  // first fighter attack, unless he is in a block stand 
  !fightStatus.pressedKeys[PlayerOneBlock] && updateFighter({
    attacker: firstFighter,
    defender: secondFighter,
    attackerButtons: playerOneButtons,
    defenderButtons: playerTwoButtons,
    healthBarId: 'right-fighter-indicator'
  })

  // second fighter attack, unless he is in a block stand
  !fightStatus.pressedKeys[PlayerTwoBlock] && updateFighter({
    attacker: secondFighter,
    defender: firstFighter,
    attackerButtons: playerTwoButtons,
    defenderButtons: playerOneButtons,
    healthBarId: 'left-fighter-indicator'
  })
} 

const onKeyUp = ({ code }) => {
  fightStatus.pressedKeys[code] = false;
};

export function fight(firstFighter, secondFighter) {
  firstFighter.currentHealth = firstFighter.health;
  firstFighter.lastCrit = null;
  secondFighter.currentHealth = secondFighter.health;
  secondFighter.lastCrit = null;

  return new Promise(resolve => {
    document.addEventListener("keydown", function onKeyDown ({ code }) {

      fightStatus.pressedKeys[code] = true;
      updateFight(firstFighter, secondFighter);
      const winner = getWinner(firstFighter, secondFighter);

      if (winner) {
        document.removeEventListener("keydown", onKeyDown);
        document.removeEventListener("keyup", onKeyUp);
        resolve(winner);
      }
    });
    document.addEventListener("keyup", onKeyUp);
  });
}

/**
 * Returns blocked damage
 * @param {number} attack 
 * @param {number} defence 
 */
export function getDamage(attack, defence) {
  const damage = attack - defence;

  if (damage < 0) {
    return 0;
  }
  return damage;
}

/**
 * Get fighter`s crit damage
 * @param {Object} fighter 
 */
export function getCritPower({ attack }) {
  return 2 * attack;
}

/**
 * Get fighter`s hit damage
 * @param {Object} fighter 
 */
export function getHitPower({ attack }) {
  return attack * (1 + Math.random());
}

/**
 * Get fighter`s block
 * @param {Object} fighter 
 */
export function getBlockPower({ defense }) {
  return defense * (1 + Math.random());
}
