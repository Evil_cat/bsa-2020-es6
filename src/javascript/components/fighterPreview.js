import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  fighter && fighterElement.append(createPreview(fighter, position));
  // todo: show fighter info (image, name, health, etc.) NOT COMPLETE

  return fighterElement;
}

function createStatPreview({ name, health, attack, defense }) {
  const statsToShow = [name, `Health: ${health}`, `Attack: ${attack}`, `Defense: ${defense}`];
  const divElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___stats',
  });

  statsToShow.forEach(text => {
    const spanElement = createElement({
      tagName: 'span',
    });
    spanElement.textContent = text;
    divElement.append(spanElement);
  });

  return divElement;
}

export function createPreview(fighter, position) {
  const divElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info',
  });
  
  divElement.append(createStatPreview(fighter));

  divElement.append(createFighterImage(fighter));

  return divElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { src: source };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    title: name,
    alt: name,
    attributes,
  });

  return imgElement;
}
