import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

const showDetails = ({ name }) => {
  const divElement = createElement({
    tagName: 'div',
    className: 'modal-details'
  });

  const spanElement = createElement({
    tagName: 'span'
  });

  spanElement.textContent = `Fighter ${name} wins the match!`;
  divElement.append(spanElement);

  return divElement;
}

export function showWinnerModal(fighter) {
  showModal({ title: 'Winner!', bodyElement: showDetails(fighter), onClose: () => document.location.reload(true) })
}
